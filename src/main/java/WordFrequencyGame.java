import java.util.*;
import java.util.stream.Collectors;


public class WordFrequencyGame {

    public static final String REMOVE_SPACE = "\\s+";


    public String getResult(String inputStr){
            try {
                String[] words = inputStr.split(REMOVE_SPACE);
                List<WordFrequency> inputList = countWords(words);
                inputList.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());
                return convertToString(inputList);
            } catch (Exception e) {
                return "Calculate Error";
            }
    }

    private static String convertToString(List<WordFrequency> inputList) {
        StringJoiner joiner = new StringJoiner("\n");
        inputList.stream().forEach(word ->{
            joiner.add(word.getValue()+ " " + word.getWordCount());
        });
        return joiner.toString();
    }


    private List<WordFrequency> countWords(String[] words){
        HashSet<String> deduplicate = new HashSet<>(List.of(words));
        return deduplicate.stream().map(word -> new WordFrequency(word,Collections.frequency(List.of(words),word)))
                .collect(Collectors.toList());
    }


}
